import struct

#--------------------------------------------
def isNum(value):
    return type(value) == type(0)

#--------------------------------------------
def isReg(value):
    return type(value) == type('') and len(value) > 0 and value[0] == '%'

#--------------------------------------------
def isStr(value):
    return type(value) == type('')

#--------------------------------------------
REGS = {
    1: ['al',  'cl',  'dl',  'bl',  'ah',  'ch',  'dh',  'bh'],
    2: ['ax',  'cx',  'dx',  'bx',  'sp',  'bp',  'si',  'di',
        'r8w', 'r9w', 'r10w', 'r11w', 'r12w', 'r13w', 'r14w', 'r15w'],
    4: ['eax', 'ecx', 'edx', 'ebx', 'esp', 'ebp', 'esi', 'edi',
        'r8d', 'r9d', 'r10d', 'r11d', 'r12d', 'r13d', 'r14d', 'r15d'],
    8: ['rax', 'rcx', 'rdx', 'rbx', 'rsp', 'rbp', 'rsi', 'rdi',
        'r8',  'r9',  'r10', 'r11', 'r12', 'r13', 'r14', 'r15']
}

#--------------------------------------------
def getRegisterIndexAndType(value):
    value = value[1:]
    for rtype, regs in REGS.iteritems():
        for idx, reg in enumerate(regs):
            if reg == value:
                return rtype, idx
    raise Exception("wrong register", value)

#--------------------------------------------
def toHex(str):
    return ":".join("{:02x}".format(ord(c)) for c in str)

#--------------------------------------------
class AsmBinGenerator:

    #--------------------------------------------
    def __init__(self):
        self.__code = ''
        self.__data = [] # (offset to insert data address, data)

    #--------------------------------------------
    def mov(self, src, dst):
        if isNum(src) and isReg(dst):
            rType, rIdx = getRegisterIndexAndType(dst)
            maxValue = 2 ** (rType * 8)
            assert src >= 0 and src < maxValue
            if rType == 1:
                self.__code += chr(0xB0 + rIdx)
                self.__code += chr(src)
            elif rType == 8:
                self.__code += chr(0x48) + chr(0xB8 + rIdx)
                self.__code += struct.pack('<Q', src)
            else:
                raise Exception("unimplemented", rType)
        elif isStr(src) and isReg(dst):
            rType, rIdx = getRegisterIndexAndType(dst)
            assert rType == 4 or rType == 8
            if rType == 8:
                self.__code += chr(0x48)
            self.__code += chr(0xB8 + rIdx)
            offset = len(self.__code)
            self.__code += rType * '\x33'
            self.__data.append((offset, src))
        else:
            raise Exception("unimplemented", src, dst)

    #--------------------------------------------
    def int(self, code):
        assert code >= 0 and code < 2 ** 8
        self.__code += chr(0xCD)
        self.__code += chr(code)

    #--------------------------------------------
    def syscall(self):
        self.__code += '\x0F\x05'


    #--------------------------------------------
    def _makeHeader(self):
        elfHeader = self._getELFHeader()
        programHeader = self._getProgramHeader()
        self.__header = elfHeader + programHeader
        assert len(elfHeader) == self.__elfHeaderSize
        assert len(programHeader) == self.__programHeaderSize
        assert len(self.__header) == self.__codeOffset

    #--------------------------------------------
    def _getELFHeader(self):
        # e_ident
        eFileClass = 2          # 1 - 32bit, 2 - 64bit
        eDataEncoding = 1       # 1 - little-endian, 2 - big-endian
        eFileVersion = 1        # 1 - EV_CURRENT
        eOsAbi = 3              # 0 - System V ABI, 1 - HP-UX os, 255 - standalone
        header = '\x7F' + 'ELF' + chr(eFileClass) + chr(eDataEncoding) + chr(eFileVersion) + chr(eOsAbi)
        header += struct.pack('<LL', 0, 0)

        # elf header
        header += struct.pack('<HHLQQQLHHHHHH',
            2,                          # e_type        object file type
            0x3E,                       # e_machine     machine type
            1,                          # e_version     object file version
            self.__entry,               # e_entry       entry point address
            self.__elfHeaderSize,       # e_phoff       program header offset
            0,                          # e_shoff       section header offset
            0,                          # e_flags       processor-specific flags
            self.__elfHeaderSize,       # e_ehsize      elf header size
            self.__programHeaderSize,   # e_phentsize   size of program header entry
            1,                          # e_phnum       number of program header entries
            64,                         # e_shentsize   size of section header entry
            0,                          # e_shnum       number of section header entries
            0                           # e_shstrndx    section name string table index
        )
        return header

    #--------------------------------------------
    def _getProgramHeader(self):
        p_type = 1              # loadable segment
        p_flags = 0x1 | 0x4     # 0x1 - execute, 0x4 - read
        return struct.pack('<LLQQQQQQ',
            p_type,                     # p_type        type of segment
            p_flags,                    # p_flags       segment attributes
            0,                          # p_offset      offset in file
            self.__startOffset,         # p_vaddr       virtual address in memory
            self.__startOffset,         # p_paddr       reserved
            self.__fullBinarySize,      # p_filesz      size of segment in file
            self.__fullBinarySize,      # p_memsz       size of segment in memory,
            0                           # p_alignment   alignment of segment
        )

    #--------------------------------------------
    def _prepareSizes(self):
        self.__startOffset = 0x10000
        self.__elfHeaderSize = 64
        self.__programHeaderSize = 56
        self.__codeOffset = self.__elfHeaderSize + self.__programHeaderSize # 120
        self.__entry = self.__startOffset + self.__codeOffset

        self.__dataLen = 0
        for _, data in self.__data:
            self.__dataLen += len(data)
        self.__fullBinarySize = self.__codeOffset + len(self.__code) + self.__dataLen

    #--------------------------------------------
    def _makeData(self):
        self.__staticData = ''
        for offset, data in self.__data:
            currentOffset = len(self.__header) + len(self.__code) + len(self.__staticData)
            self.__staticData += data
            offsetBytes = struct.pack("<Q", currentOffset + self.__startOffset)
            self.__code = self.__code[:offset] + offsetBytes + self.__code[offset + 8:]

    #--------------------------------------------
    def assemble(self):
        self._prepareSizes()
        self._makeHeader()
        self._makeData()

        binaryData = self.__header + self.__code + self.__staticData

        assert len(binaryData) == self.__fullBinarySize
        return binaryData

#--------------------------------------------
if __name__ == '__main__':
    gen = AsmBinGenerator()
    gen.mov(1, '%rax')
    gen.mov(1, '%rdi')
    str = 'Still works?!\n'
    gen.mov(str, '%rsi')
    gen.mov(len(str), '%rdx')
    gen.syscall()

    gen.mov(60, '%rax')
    gen.mov(0, '%rdi')
    gen.syscall()
    data = gen.assemble()
    #print toHex(data)
    open('dump.bin', 'wb').write(data)
