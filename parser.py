#!/usr/bin/env python

import ply.yacc as yacc
import ast
from lexer import Lexer
import ast_visualizer

#--------------------------------------------
class Parser:
    #--------------------------------------------
    def __init__(self):
        self.__lexer = Lexer()
        self.tokens = self.__lexer.tokens
        self.__parser = yacc.yacc(module=self)
        self.__isError = False

    #--------------------------------------------
    def parse(self, data):
        return self.optimize(self.__parser.parse(data, self.__lexer, debug=0))

    #--------------------------------------------
    def p_statements_empty(self, p):
        "statements : "
        p[0] = ast.SequenceNode()

    #--------------------------------------------
    def p_statements_single(self, p):
        "statements : statement"
        p[0] = ast.SequenceNode(p[1])

    #--------------------------------------------
    def p_statements_multi(self, p):
        "statements : statements NL statements"
        p[0] = sequenceNode = p[1]
        sequenceNode.addNode(p[3])

    #--------------------------------------------
    def p_statements_indent(self, p):
        "block : INDENT statements DEDENT"
        p[0] = p[2]

    #--------------------------------------------
    def p_block(self, p):
        "statements : block"
        p[0] = p[1]

    # --------------------------------------------
    def p_func_call(self, p):
        "func : NAME '(' args ')'"
        p[0] = ast.FunctionCallNode(p[1], p[3])

    # Function processing
    #--------------------------------------------
    def p_statement_call(self, p):
        "statement : func"
        p[0] = p[1]

    #--------------------------------------------
    def p_args_empty(self, p):
        "args : "
        p[0] = []

    #--------------------------------------------
    def p_args_expr(self, p):
        "args : expr"
        p[0] = [p[1],]

    #--------------------------------------------
    def p_args_several(self, p):
        "args : args ',' args"
        p[0] = p[1] + p[3]

    #--------------------------------------------
    def p_statement_func(self, p):
        "statement : FUNC NAME '(' args ')' NL block"
        p[0] = ast.FunctionNode(p[2], p[4], p[7])


    #--------------------------------------------
    def p_statement_asign(self, p):
        "statement : NAME '=' expr"
        variable = ast.VariableNode(p[1])
        p[0] = ast.AssignNode(variable, p[3])

    # Function processing
    # --------------------------------------------
    def p_statement_assign_call(self, p):
        "statement : NAME '=' func"
        variable = ast.VariableNode(p[1])
        p[0] = ast.AssignNode(variable, p[3])

    #--------------------------------------------
    def p_statement_if(self, p):
        "statement : IF expr NL block"
        p[0] = ast.IfNode(p[2], p[4])

    #--------------------------------------------
    def p_statement_for(self, p):
        "statement : FOR statement ';' expr NL block"
        p[0] = ast.ForNode(p[2], p[4], p[6])

    #   Math expressions processing
    #----------------------------------------------------------------
    def p_expr_plus(self, p):
        "expr : expr '+' expr"
        p[0] = ast.BinOpNode('+', p[1], p[3])

    #----------------------------------------------------------------
    def p_expr_minus(self, p):
        "expr : expr '-' expr"
        p[0] = ast.BinOpNode('-', p[1], p[3])

    #----------------------------------------------------------------
    def p_expr_mul(self, p):
        "expr : expr '*' expr"
        p[0] = ast.BinOpNode('*', p[1], p[3])

    #----------------------------------------------------------------
    def p_expr_div(self, p):
        "expr : expr '/' expr"
        p[0] = ast.BinOpNode('/', p[1], p[3])

    #----------------------------------------------------------------
    def p_expr_more(self, p):
        "expr : expr '>' expr"
        p[0] = ast.BinOpNode('>', p[1], p[3])

    #----------------------------------------------------------------
    def p_expr_more_equal(self, p):
        "expr : expr '>' '=' expr"
        p[0] = ast.BinOpNode('>=', p[1], p[4])

    #----------------------------------------------------------------
    def p_expr_less(self, p):
        "expr : expr '<' expr"
        p[0] = ast.BinOpNode('<', p[1], p[3])

    #----------------------------------------------------------------
    def p_expr_less_equal(self, p):
        "expr : expr '<' '=' expr"
        p[0] = ast.BinOpNode('<=', p[1], p[4])

    #----------------------------------------------------------------
    def p_expr_equal(self, p):
        "expr : expr '=' '=' expr"
        p[0] = ast.BinOpNode('==', p[1], p[4])

    #----------------------------------------------------------------
    def p_expr_not_equal(self, p):
        "expr : expr '!' '=' expr"
        p[0] = ast.BinOpNode('!=', p[1], p[4])

    #----------------------------------------------------------------
    def p_expression_uminus(self, p):
        "expr : '-' expr %prec UMINUS"
        p[0] = -p[2]

    # Braces processing
    #----------------------------------------------------------------
    def p_expr_brace(self, p):
        "expr : '(' expr ')'"
        p[0] = p[2]

    #   Numbers processing
    #----------------------------------------------------------------
    def p_expr_num(self, p):
        "expr : INT"
        p[0] = ast.ConstantNode(p[1], 'int')

    #--------------------------------------------
    def p_expr_float(self, p):
        "expr : FLOAT"
        p[0] = ast.ConstantNode(p[1], 'float')

    #--------------------------------------------
    def p_expr_name(self, p):
        "expr : NAME"
        p[0] = ast.VariableNode(p[1])

    #--------------------------------------------
    def p_error(self, p):
        global _g_isError
        if p != None:
            _g_isError = True
            print 'Syntax error:', p
            #print "line %d: Syntax error at '%s'" % (generator.getCurrentLineNumber(), p.value)

    #--------------------------------------------
    def hasErrors(self):
        return self.__isError

    #--------------------------------------------
    precedence = (
        ('left','+','-'),
        ('left','*','/'),
        ('right', 'UMINUS'),
    )

    #--------------------------------------------
    def optimize(self, tree):
        for i in xrange(0, len(tree._children)):
            tree._children[i] = self.optimize(tree._children[i])

        if tree.getType() == 'sequence' and len(tree._children) == 1:
            return tree._children[0]

        return tree

#--------------------------------------------
if __name__ == '__main__':
    parser = Parser()
    data = open('examples/test.fly').read()
    tree = parser.parse(data)
    ast_visualizer.drawAst(tree, 'tree.png')
