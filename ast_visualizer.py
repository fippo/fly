import pygraphviz as pgv
from collections import defaultdict

_g_counter = 0
_g_levels = defaultdict(list)
_g_typeToColor = {
    'constant': '#CCFFFF',
    'variable': '#CCFFCC',
    'sequence': '#FFCC66',
}

def processNode(node, graph, level):
    global _g_counter
    global _g_levels
    _g_counter += 1
    nodeId = _g_counter

    graph.add_node(_g_counter,
                   fillcolor=_g_typeToColor.get(node.getType(), '#FFCC99'),
                   style='filled,rounded,bold',
                   label=node.toStr(),
                   shape='box',
                   penwidth=1)

    childrenIds = []

    for child in node._children:
        childId = processNode(child, graph, level + 1)
        graph.add_edge(nodeId, childId)
        childrenIds.append(childId)

    _g_levels[level] += childrenIds

    return nodeId

def drawAst(rootNode, fileName):
    global _g_counter
    global _g_levels
    _g_counter = 1
    _g_levels = defaultdict(list)

    graph = pgv.AGraph()
    processNode(rootNode, graph, 0)
    graph.add_subgraph([1,], rank='same')

    for level in sorted(_g_levels.keys()):
        graph.add_subgraph(_g_levels[level], rank='same')

    graph.draw(fileName, prog='dot')
