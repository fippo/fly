from parser import Parser
from mid_code_generator import Generator as MidCodeGenerator
from collections import defaultdict
from ast_visualizer import drawAst

class RegAllocator:
    REGISTER_NAMES = ['rbx', 'rcx', 'rdx', 'rex', 'r8', 'r9', 'r10', 'r11', 'r12', 'r13', 'r14', 'r15', 'r16']

    def __init__(self, instructions):
        self.__instructions = instructions
        self.__usages = defaultdict(list)
        self.__realRegToReg = {}
        self.__regToRealReg = {}

    def allocate(self):
        self._collectRegUsages()
        self._process()

    def _deallocateUnusedRegisters(self, currentLine):
        for reg, realReg in self.__regToRealReg.items():
            if self.__usages[reg][-1] < currentLine:
                del self.__regToRealReg[reg]
                del self.__realRegToReg[realReg]

    def _allocateNewReg(self, reg):
        # we have already selected real register
        if reg in self.__regToRealReg:
            return

        # select first empty register
        for realReg in self.REGISTER_NAMES:
            if not realReg in self.__realRegToReg:
                self.__realRegToReg[realReg] = reg
                self.__regToRealReg[reg] = realReg
                return

        # todo: implement stack store operation if no free register found
        raise Exception("No free register found")

    def _process(self):
        for lineNum, inst in enumerate(self.__instructions):
            self._deallocateUnusedRegisters(lineNum)
            resultReg = inst.getResultReg()
            if resultReg is not None:
                self._allocateNewReg(resultReg)
                inst.setResultReg(self.__regToRealReg[resultReg])
            args = inst.getArgs()
            for i, arg in enumerate(args):
                if type(arg) == type('') and arg.startswith('r'):
                    args[i] = self.__regToRealReg[arg]

    def _collectRegUsages(self):
        for lineNum, inst in enumerate(self.__instructions):
            regs = inst.getRegs()
            for reg in regs:
                self.__usages[reg].append(lineNum)


def allocate(midCode):
    for instructions in midCode.itervalues():
        regAllocator = RegAllocator(instructions)
        regAllocator.allocate()

#--------------------------------------------
if __name__ == '__main__':
    generator = MidCodeGenerator()
    parser = Parser()
    data = open('examples/test.fly').read()
    tree = parser.parse(data)
    drawAst(tree, 'tree.png')
    midCode = generator.generate(tree)
    allocate(midCode)
    for inst in midCode['main']:
        inst.dump()
