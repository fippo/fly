#!/usr/bin/env python

import sys
import commands
from parser import Parser
from mid_code_generator import Generator as GeneratorMidCode
from llvm_generator import GeneratorLLVM
from ast_visualizer import drawAst

class FlyCompiler(object):
    def __init__(self):
        self.__parser = Parser()
        self.__midCodeGenerator = GeneratorMidCode()
        self.__llvmGenerator = GeneratorLLVM()

    def compile(self, inputFile):
        data = open(inputFile).read()
        tree = self.__parser.parse(data)
        drawAst(tree, 'tree.png')
        midCode = self.__midCodeGenerator.generate(tree)
        self.__llvmGenerator.generate(midCode, 'temp.ll')
        out = commands.getstatusoutput('clang temp.ll')[1]
        if out:
            print out,

def main():
    if len(sys.argv) != 2:
        print 'Usage: %s inputFile.fly'
        sys.exit(42)

    inputFile = sys.argv[1]
    compiler = FlyCompiler()
    compiler.compile(inputFile)

if __name__ == '__main__':
    main()
