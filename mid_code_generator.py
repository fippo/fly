from parser import Parser
import ast_visualizer

#--------------------------------------------
class Instruction:
    def __init__(self, type, *args):
        self.__type = type
        self.__args = args

    def getType(self):
        return self.__type

    def getArgs(self):
        return self.__args

    def dump(self):
        print self.__type, ' '.join([str(arg) for arg in self.__args])

#--------------------------------------------
class Generator:
    def __init__(self):
        self.__functions = {}   # func name => list of instructions
        self.__regCounter = 1
        self.__labelCounter = 0

    def __processNode(self, node):
        nodeType = node.getType()

        if nodeType == 'sequence':
            commands = []
            for child in node._children:
                if child.getType() in ('call', 'binop'):
                    commands += self.__processNode(child)[1]
                else:
                    commands += self.__processNode(child)
            return commands

        if nodeType == 'func':
            args = node.getArguments()
            body = node.getBody()
            self.__allocatedVars = set()
            self.__regCounter = 1
            commands = []
            resArgs = []
            for arg in args:
                resArgs.append(arg.getName())
                #varName = arg.getName()
                #commands.append(Instruction('POP', self.__regCounter, [varName,]))
                #self.__varToReg[varName] = self.__regCounter
                #self.__regCounter += 1
            commands.append(Instruction('FUNC', node.getName(), *resArgs))
            if body.getType() in ('call', 'binop'):
                commands += self.__processNode(body)[1]
            else:
                commands += self.__processNode(body)
            commands.append(Instruction('RET'))
            self.__functions[node.getName()] = commands
            return commands

        if nodeType == 'assign':
            varName = node.getVariable().getName()
            expr = node.getExpression()
            commands = []
            exprType = expr.getType()
            if varName not in self.__allocatedVars:
                commands.append(Instruction('ALLOC', varName))
                self.__allocatedVars.add(varName)
            if exprType == 'constant':
                commands.append(Instruction('STOREC', varName, expr.getValue()))
            elif exprType == 'variable':
                commands.append(Instruction('LOAD', expr.getName(), self.__regCounter))
                commands.append(Instruction('STORE', varName, self.__regCounter))
                self.__regCounter += 1
            elif exprType in ('call', 'binop'):
                binopResultReg, binOpCommnds = self.__processNode(expr)
                commands += binOpCommnds
                commands.append(Instruction('STORE', varName, binopResultReg))
            else:
                assert False

            return commands

        if nodeType == 'binop':
            left = node.getLeft()
            right = node.getRight()
            leftType = left.getType()
            rightType = right.getType()
            opName = node.getOpName()

            commands = []
            if leftType == 'constant':
                leftResult = left.getValue()
            elif leftType == 'variable':
                commands.append(Instruction('LOAD', left.getName(), self.__regCounter))
                leftResult = 'r' + str(self.__regCounter)
                self.__regCounter += 1
            elif leftType == 'binop':
                leftResult, binOpCommnds = self.__processNode(left)
                leftResult = 'r' + str(leftResult)
                commands += binOpCommnds
            else:
                assert False

            if rightType == 'constant':
                rightResult = right.getValue()
            elif rightType == 'variable':
                commands.append(Instruction('LOAD', right.getName(), self.__regCounter))
                rightResult = 'r' + str(self.__regCounter)
                self.__regCounter += 1
            elif rightType == 'binop':
                rightResult, binOpCommnds = self.__processNode(right)
                rightResult = 'r' + str(rightResult)
                commands += binOpCommnds
            else:
                assert False

            if opName == '+':
                instName = 'SUM'
            elif opName == '-':
                instName = 'DIFF'
            elif opName == '*':
                instName = 'MUL'
            elif opName == '/':
                instName = 'DIV'
            elif opName == '>':
                instName = 'MORE'
            elif opName == '<':
                instName = 'LESS'
            else:
                assert False

            resRegCounter = self.__regCounter
            self.__regCounter += 1
            commands.append(Instruction(instName, resRegCounter, leftResult, rightResult))
            return resRegCounter, commands

        if nodeType == 'if':
            condition = node.getCondition()
            ifBody = node.getIfBody()
            assert condition.getType() == 'binop'
            conditionReg, commands = self.__processNode(condition)
            labelTrue = self.__labelCounter
            self.__labelCounter += 1
            labelFalse = self.__labelCounter
            self.__labelCounter += 1
            commands.append(Instruction('JMP', conditionReg, labelTrue, labelFalse))
            commands.append(Instruction('LABEL', labelTrue))
            if ifBody.getType() == 'call':
                commands += self.__processNode(ifBody)[1]
            else:
                commands += self.__processNode(ifBody)
            commands.append(Instruction('JMP', labelFalse))
            commands.append(Instruction('LABEL', labelFalse))
            return commands

        if nodeType == 'call':
            commands = []
            funcName = node.getName()
            args = []
            for child in node._children:
                childType = child.getType()
                if childType == 'variable':
                    commands.append(Instruction('LOAD', child.getName(), self.__regCounter))
                    reg = 'r' + str(self.__regCounter)
                    self.__regCounter += 1
                    args.append(reg)
                elif childType == 'constant':
                    args.append(child.getValue())
                else:
                    assert False

            resRegCounter = self.__regCounter
            self.__regCounter += 1
            commands.append(Instruction('CALL', resRegCounter, funcName, *args))
            return resRegCounter, commands

        assert False


    def generate(self, tree):
        self.__processNode(tree)
        return self.__functions

#--------------------------------------------
if __name__ == '__main__':
    generator = Generator()
    parser = Parser()
    data = open('examples/test.fly').read()
    tree = parser.parse(data)
    ast_visualizer.drawAst(tree, 'tree.png')
    midCode = generator.generate(tree)

    for funcName in midCode.keys():
        for inst in midCode[funcName]:
            inst.dump()
        print
    print
