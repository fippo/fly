from parser import Parser
import ast_visualizer
from mid_code_generator import Generator


class GeneratorLLVM(object):

    def __init__(self, templateFile='template.ll'):
        self.__baseLines = []
        with open(templateFile, 'r') as f:
            for line in f:
                self.__baseLines.append(line.strip())

    def generate(self, midCode, outFile):
        lines = list(self.__baseLines)
        for funcName in midCode.keys():
            for inst in midCode[funcName]:
                lines += self.__processInstruction(inst)

        if outFile is not None:
            with open(outFile, 'w') as f:
                for l in lines:
                    f.write(l + '\n')
        else:
            for l in lines:
                print l

    def __getArg(self, arg):
        if isinstance(arg, int):
            return str(arg)
        if arg.startswith('r'):
            return '%' + arg[1:]
        raise Exception('not implemented')

    def __processInstruction(self, inst):
        instType = inst.getType()
        instArgs = inst.getArgs()
        commands = []
        if instType == 'ALLOC':
            commands.append('%s = alloca i32, align 4' % ('%' + instArgs[0]))
        elif instType == 'STOREC':
            commands.append('store i32 %d, i32* %s' % (instArgs[1], '%' + instArgs[0]))
        elif instType == 'STORE':
            commands.append('store i32 %s, i32* %s' % ('%' + str(instArgs[1]), '%' + instArgs[0]))
        elif instType == 'LOAD':
            commands.append('%s = load i32* %s' % ('%' + str(instArgs[1]), '%' + instArgs[0]))
        elif instType == 'SUM':
            commands.append('%s = add i32 %s, %s' % ('%' + str(instArgs[0]),
                                           self.__getArg(instArgs[1]),
                                           self.__getArg(instArgs[2])))
        elif instType == 'MUL':
            commands.append('%s = mul i32 %s, %s' % ('%' + str(instArgs[0]),
                                           self.__getArg(instArgs[1]),
                                           self.__getArg(instArgs[2])))
        elif instType == 'DIFF':
            commands.append('%s = sub i32 %s, %s' % ('%' + str(instArgs[0]),
                                           self.__getArg(instArgs[1]),
                                           self.__getArg(instArgs[2])))
        elif instType == 'MORE':
            commands.append('%s = icmp sgt i32 %s, %s' % ('%' + str(instArgs[0]),
                                                self.__getArg(instArgs[1]),
                                                self.__getArg(instArgs[2])))
        elif instType == 'JMP':
            if len(instArgs) == 3:
                commands.append('br i1 %s, label %s, label %s' % ('%' + str(instArgs[0]),
                                                        '%l' + str(instArgs[1]),
                                                        '%l' + str(instArgs[2])))
            else:
                commands.append('br label %s' % '%l' + str(instArgs[0]))
        elif instType == 'LABEL':
            commands.append('l%s:' % str(instArgs[0]))
        elif instType == 'CALL':
            resultReg = '%' + str(instArgs[0])
            funcName = instArgs[1]
            res = "%s = call i32 @%s(" % (resultReg, funcName)
            for i in xrange(2, len(instArgs)):
                if i > 2:
                    res += ', '
                res += 'i32 ' + self.__getArg(instArgs[i])
            res += ')'
            commands.append(res)
        elif instType == 'FUNC':
            funcArgs = instArgs[1:]
            argStr = ', '.join([('i32 %$' + arg) for arg in funcArgs])
            commands.append('define i32 @%s(%s) #0 {' % (instArgs[0], argStr))
            for arg in funcArgs:
                commands.append('%s = alloca i32, align 4' % ('%' + arg))
                commands.append('store i32 %s, i32* %s, align 4' % ('%$' + arg, '%' + arg))
        elif instType == 'RET':
            commands.append('ret i32 0')
            commands.append('}')
        else:
            commands.append('unknown ' + instType + ' ' + ' '.join([str(arg) for arg in instArgs]))
        return commands

if __name__ == '__main__':
    generator = Generator()
    generatorLlvm = GeneratorLLVM()
    parser = Parser()
    data = open('examples/test.fly').read()
    tree = parser.parse(data)
    ast_visualizer.drawAst(tree, 'tree.png')
    midCode = generator.generate(tree)
    generatorLlvm.generate(midCode, None)
