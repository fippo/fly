#--------------------------------------------
class Node:
    def __init__(self, children = None):
        if children is not None:
            self._children = children
        else:
            self._children = []

    def getType(self):
        return "node"

    def getValue(self):
        return None

    def getValueType(self):
        return None

    def toStr(self):
        res = self.getType()
        type = self.getValueType()
        value = self.getValue()
        typeValue = ''
        if type is not None:
            typeValue = type
            if value is not None:
                typeValue += ' '
        if value is not None:
            typeValue += str(value)
        if len(typeValue) > 0:
            res += '\n' + typeValue + ''
        return res

#--------------------------------------------
class TypedNode(Node):
    def __init__(self, valueType):
        Node.__init__(self)
        self.__valueType = valueType

    def getValueType(self):
        return self.__valueType

#--------------------------------------------
class ConstantNode(TypedNode):
    def __init__(self, value, valueType):
        TypedNode.__init__(self, valueType)
        self.__value = value

    def getType(self):
        return "constant"

    def getValue(self):
        return self.__value

#--------------------------------------------
class BinOpNode(Node):
    def __init__(self, opName, leftNode, rightNode):
        Node.__init__(self, [leftNode, rightNode])
        self.__opName = opName

    def getType(self):
        return "binop"

    def getOpName(self):
        return self.__opName

    def getValue(self):
        return self.__opName

    def getLeft(self):
        return self._children[0]

    def getRight(self):
        return self._children[1]

#--------------------------------------------
class VariableNode(Node):
    def __init__(self, variableName):
        Node.__init__(self)
        self.__name = variableName

    def getType(self):
        return "variable"

    def getName(self):
        return self.__name

    def getValue(self):
        return self.__name

#--------------------------------------------
class AssignNode(Node):
    def __init__(self, variable, expression):
        Node.__init__(self, [variable, expression])

    def getType(self):
        return "assign"

    def getVariable(self):
        return self._children[0]

    def getExpression(self):
        return self._children[1]

#--------------------------------------------
class IfNode(Node):
    def __init__(self, condition, ifBody, elseBody = None):
        if elseBody is None:
            Node.__init__(self, [condition, ifBody])
        else:
            Node.__init__(self, [condition, ifBody, elseBody])

    def getType(self):
        return "if"

    def getCondition(self):
        return self._children[0]

    def getIfBody(self):
        return self._children[1]

    def getElseBody(self):
        return None if len(self._children) == 2 else self._children[1]

#--------------------------------------------
class ForNode(Node):
    def __init__(self, initializer, condition, body):
        Node.__init__(self, [initializer, condition, body])

    def getType(self):
        return "for"

    def getInitializer(self):
        return self._children[0]

    def getCondition(self):
        return self._children[1]

    def getBody(self):
        return self._children[2]

#--------------------------------------------
class SequenceNode(Node):
    def __init__(self, initialNode = None):
        if initialNode is not None:
            Node.__init__(self, [initialNode,])
        else:
            Node.__init__(self)

    def getType(self):
        return "sequence"

    def addNode(self, node):
        if node.getType() == 'sequence':
            self._children += node._children
        else:
            self._children.append(node)

#--------------------------------------------
class FunctionCallNode(Node):
    def __init__(self, name, arguments):
        Node.__init__(self, arguments)
        self.__name = name

    def getType(self):
        return "call"

    def getName(self):
        return self.__name

    def getValue(self):
        return self.__name

#--------------------------------------------
class FunctionNode(Node):
    def __init__(self, name, arguments, body):
        Node.__init__(self, arguments + [body,])
        self.__name = name

    def getType(self):
        return "func"

    def getName(self):
        return self.__name

    def getValue(self):
        return self.__name

    def getArguments(self):
        return self._children[:-1]

    def getBody(self):
        return self._children[-1]

#--------------------------------------------
class AST:
    def __init__(self):
        pass

