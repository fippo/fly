#!/usr/bin/env python

import ply.lex as lex

class Lexer:
    #--------------------------------------------
    def __init__(self, **kwargs):
        self.__isError = False
        self.__lexer = lex.lex(object=self, **kwargs)

    #--------------------------------------------
    def input(self, data):
        lines = data.split('\n')
        newLines = []
        for line in lines:
            newLines.append(self.__replaceSpaces(line.rstrip()))
        data = '\n'.join(newLines)
        self.__lexer.input(data)
        self.__tokenStream = iter(self.__lexer.token, False)
        self.__tokenStream = self.addEnd(self.__tokenStream)
        self.__tokenStream = self.processIndents(self.__tokenStream)

    # --------------------------------------------
    def __replaceSpaces(self, line):
        n = 0
        while n < len(line) and (line[n] == ' ' or line[n] == '\t'):
            if line[n] == '\t':
                raise Exception('tabs not allowed: %s' % line)
            n += 1
        if n % 4 != 0:
            raise Exception('wrong indention: %s' % line)
        return ('\t' * (n / 4)) + line[n:]

    #--------------------------------------------
    def token(self):
        return self.__tokenStream.next()

    #--------------------------------------------
    def __iter__(self):
        return self.__tokenStream

    #--------------------------------------------
    def addEnd(self, tokenStream):
        wasNl = False
        for token in tokenStream:
            if token == None:
                if not wasNl:
                    yield self.getToken('NL')
                yield self.getToken('END')
            elif token.type == 'NL':
                wasNl = True
            else:
                wasNl = False
            yield token

    #--------------------------------------------
    def processIndents(self, tokenStream):
        currentDepth = 0
        depth = 0
        wasNL = False
        for token in tokenStream:
            if token == None:
                yield token
                continue

            if token.type == 'TB':
                depth = len(token.value)
                continue

            if token.type == 'NL':
                depth = 0
                if not wasNL:
                    yield token
                wasNL = True
                continue

            wasNL = False

            while depth > currentDepth:
                currentDepth += 1
                yield self.getToken('INDENT', currentDepth)

            while depth < currentDepth:
                currentDepth -= 1
                yield self.getToken('DEDENT', currentDepth)
                yield self.getToken('NL', '\n')

            if token and token.type == 'END':
                continue
            yield token

    #--------------------------------------------
    def getToken(self, type, value = None):
        tok = lex.LexToken()
        tok.type = type
        tok.value = value
        tok.lineno = 0
        tok.lexpos = 0
        return tok

    #--------------------------------------------
    keywords = (
        'IF',
        'FOR',
        'FUNC',
    )

    #--------------------------------------------
    tokens = keywords + (
        'FLOAT',
        'INT',
        'NAME',
        'NL',
        'TB',
        'INDENT',
        'DEDENT',
        'END',
    )

    #--------------------------------------------
    t_NL = r'\n+'
    t_TB = r'\t+'

    #--------------------------------------------
    literals = ['+','-','*','/', '(',')','=', '{', '}', '>', '<', ',', '!', ';']

    #--------------------------------------------
    def t_NAME(self, t):
        r'[a-zA-Z_][a-zA-Z0-9_]*'
        val = t.value.upper()
        if val in self.keywords:
            t.type = val
        return t

    #--------------------------------------------
    def t_FLOAT(self, t):
        r'\d+\.\d+'
        t.value = float(t.value)
        return t

    #--------------------------------------------
    def t_INT(self, t):
        r'\d+'
        t.value = int(t.value)
        return t

    #--------------------------------------------
    def t_error(self, t):
        global _g_isError
        _g_isError = True
        print "Illegal character '%s'" % t.value[0]
        t.lexer.skip(1)

    #--------------------------------------------
    def hasErrors(self):
        return self.__isError

    #--------------------------------------------
    t_ignore = ' '

#--------------------------------------------
if __name__ == '__main__':
    lexer = Lexer()
    data = open('examples/test.fly').read()
    lexer.input(data)
    for token in lexer:
        if token == None:
            break
        print token
