; ModuleID = 'main.c'

@.str = private unnamed_addr constant [8 x i8] c"Out:%d\0A\00", align 1
@.str1 = private unnamed_addr constant [5 x i8] c"In: \00", align 1
@.str2 = private unnamed_addr constant [3 x i8] c"%d\00", align 1

define i32 @print(i32 %val) #0 {
  %1 = alloca i32, align 4
  store i32 %val, i32* %1, align 4
  %2 = load i32* %1, align 4
  %3 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([8 x i8]* @.str, i32 0, i32 0), i32 %2)
  ret i32 0
}

define i32 @scan() #0 {
  %res = alloca i32, align 4
  %1 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([5 x i8]* @.str1, i32 0, i32 0))
  %2 = call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds ([3 x i8]* @.str2, i32 0, i32 0), i32* %res)
  %3 = load i32* %res, align 4
  ret i32 %3
}
declare i32 @scanf(i8*, ...) #1
declare i32 @printf(i8*, ...) #1

